require 'spec_helper'

describe "Static Pages >" do

    subject { page }

    describe "Home Page >" do

        before { visit root_path }

        it { should have_content('Home Page') }

        it { should have_selector('title', :text => full_title("")) }

        it { should_not have_selector('title', :text => "| Home") }

    end

    describe "Help Page >" do

        before { visit help_path }

        it { should have_content('Help Page') }

        it { should have_selector('title', :text => full_title("Help")) }

    end

    describe "About Page >" do

        before { visit about_path }

        it { should have_content('About') }

        it { should have_selector('title', :text => full_title("About")) }

    end

    describe "Contact >" do

        before { visit contact_path }

        it { should have_content('Contact') }

        it { should have_selector('title', :text => full_title("Contact")) }

    end

    it "should have the right links on the layout" do
        visit root_path
        click_link "About"
        page.should have_selector('title', :text => "About")
        click_link "Contact"
        page.should have_selector('title', :text => "Contact")
        click_link "Help"
        page.should have_selector('title', :text => "Help")
        click_link "sample app"
        page.should have_selector('h1', :text => "Home Page")
        click_link "Get started today!"
        page.should have_selector('title', :text => "Signup")
    end
end
