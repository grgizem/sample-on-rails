require 'spec_helper'

describe "UserPages" do

    subject { page }

    describe "Signup Page >" do

        before { visit signup_path }

        it { should have_content('Signup') }

        it { should have_selector('title', :text => full_title("Signup")) }

    end

end
