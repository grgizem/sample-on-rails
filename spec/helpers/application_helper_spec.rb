require 'spec_helper'

describe ApplicationHelper do
    describe "full_title" do

        it "should include the page title" do
            full_title("anything").should =~ /anything/
        end

        it "should include the base title" do
            full_title("anything").should =~ /^New Application/
        end

        it "should not include the bar in home page" do
            full_title("").should_not =~ /\|/
        end
    end

end
