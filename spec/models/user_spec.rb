require 'spec_helper'

describe User do

    before { @user = User.new(name: "Gizem Gur", email: "grgizem@gmail.com",
                              password: "mostsecurepass", password_confirmation: "mostsecurepass") }

    subject { @user }

    it { should respond_to(:name) }
    it { should respond_to(:email) }
    it { should respond_to(:password_digest) }
    it { should respond_to(:password) }
    it { should respond_to(:password_confirmation) }
    it { should respond_to(:authenticate) }

    it { should be_valid }

    describe "when the name is not present" do
        before { @user.name = " " }
        it { should_not be_valid }
    end

    describe "when the email is not present" do
        before { @user.email = " " }
        it { should_not be_valid }
    end

    describe "when the name is too long" do
        before { @user.name = "a" * 51 }
        it { should_not be_valid }
    end

    describe "when the email format is invalid" do
        it "should be invalid" do
            addresses = %w[users@foo,bar users_at_foor.bar example.user@foo.]
            addresses.each do |invalid_address|
                @user.email = invalid_address
                @user.should_not be_valid
            end
        end
    end

    describe "when the email format is valid" do
        it "should be valid" do
            addresses = %w[users@foo.bar users_at_@foor.bar example._user@foo.co]
            addresses.each do |valid_address|
                @user.email = valid_address
                @user.should be_valid
            end
        end
    end

    describe "when the email is already taken" do
        before do
            @user_with_same_email = @user.dup
            @user_with_same_email.save
        end
        it { should_not be_valid }
    end

    describe "when one of the password is not present" do
        before { @user.password = @user.password_confirmation = " " }
        it { should_not be_valid }
    end

    describe "when the password is not match with confirmation" do
        before { @user.password_confirmation = "notmatchingpass" }
        it { should_not be_valid }
    end

    describe "when the password confirmation is nil" do
        before { @user.password_confirmation = nil }
        it { should_not be_valid }
    end

    describe "when the password is too short" do
        before { @user.password = "a" * 5 }
        it { should_not be_valid }
    end

    describe "return value of the authentication method" do
        before { @user.save }
        let(:found_user) { User.find_by_email(@user.email) }

        describe "with valid password" do
            it { should == found_user.authenticate(@user.password) }
        end

        describe "with invalid password" do
            let(:user_with_invalid_password) {found_user.authenticate("invalidpass")}
            it { should_not == user_with_invalid_password }
            specify { user_with_invalid_password.should be_false }
        end
    end
end
