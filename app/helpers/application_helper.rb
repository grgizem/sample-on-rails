module ApplicationHelper

# Returns the full title on the per-page basis.
def full_title(page_title)
    base_title = "New Application"
    if page_title.empty?
        base_title
    else
        "#{base_title} | #{page_title}"
    end
end

end
